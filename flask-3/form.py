from flask import Flask, render_template
from flask_wtf import FlaskForm
from wtforms.fields.core import Label
from wtforms import StringField, PasswordField, SubmitField
from wtforms.validators import ValidationError, DataRequired, Length, Email


app = Flask(__name__)
app.config['SECRET_KEY']='!ni$3cre7Key'

class UserForm(FlaskForm):
    username = StringField(label=('Masukkan Nama:'), validators=[DataRequired(),
                                                                 Length(min=5, max=25, 
                                                                        message='Nama tidak boleh kurang dari %(min)d dan lebih dari %(max)d')])
    
    submit = SubmitField(label=('Submit'))
    
    
class CreateUser(FlaskForm):
    username = StringField(label=('Masukkan Nama:'), validators=[DataRequired(),
                                                                 Length(min=5, max=25, 
                                                                        message='Nama tidak boleh kurang dari %(min)d dan lebih dari %(max)d')])
    email = StringField(label=('Masukkan Email'), validators=[DataRequired(), Email(), 
                                                              Length(min=8, max=25,
                                                                     message='Email tidak boleh kurang dari %(min)d dan lebih dari %(max)d')])
    password = PasswordField(label=('Masukkan Password'), validators=[DataRequired(), 
                                                                      Length(min=8, max=25,
                                                                     message='Password tidak boleh kurang dari %(min)d dan lebih dari %(max)d')])
    
    submit = SubmitField(label=('Submit'))
    

@app.route('/', methods=['GET', 'POST'])
def index():
    form = CreateUser()
    if form.validate_on_submit():
        return f"""<h1> selamat datang {form.username.data} </h1>"""
    return render_template('register.html', form=form)