from flask import Flask, request, Blueprint
from flask_restful import Resource, Api

from resources.messages import messages_api


import models
# import messages

app = Flask(__name__)
api = Api(app)

users = {}

# app.register_blueprint(messages_api)
app.register_blueprint(messages_api, url_prefix= '/api/v1')

class User(Resource):
    def get(self, user_id):
        return {'name': users[user_id]}
    
    def put(self, user_id):
        users[user_id] = request.form['user']
        return {'name': users[user_id]}

api.add_resource(User, '/user/<string:user_id>')
# api.add_resource(messages.MessageList, '/messages')

if __name__ == '__main__':
    models.initialize()
    app.run(debug=True)

