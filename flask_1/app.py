import os
from flask import (Flask, render_template, request, url_for, redirect, session, 
                   make_response ,abort, flash, send_file, send_from_directory)
from werkzeug.utils import secure_filename

app = Flask(__name__)
app.secret_key = '6de0ea0973ac6855c367514e34b09837'

@app.errorhandler(401)
def page_401(e):
    return render_template('error_page/401.html'), 401

# @app.route('/')
# def index():
#     return 'Haloo Peseta Flask'

# @app.route('/home')
# def show_home():
#     return render_template('home.html')

@app.route('/')
def show_home():
    search = request.args.get('search')
    return render_template('home.html', search=search)

# @app.route('/profile/<username>')
# def show_profile(username):
#     return ('Halo kamu di halaman profile %s' % username)

@app.route('/blog/<int:blog_id>')
def show_blog(blog_id):
    return 'Halo kamu di blog %s' % blog_id

@app.route('/profile/<username>')
def show_profile(username):
    return render_template('profile.html', username=username)

@app.route('/login', methods=['GET', 'POST'])
def show_login():
    if request.method == 'POST':
        resp = make_response('Email kamu adalah '+ request.form['email'])
        resp.set_cookie('email_user', request.form['email'])
        return resp
        
        # if request.form['password'] == '':
        #     abort(401)
        
        # session['username'] = request.form['email']
        # flash('kamu berhasil login', 'success')
        # return redirect(url_for('show_profile', username=session['username']))
    return render_template('login.html')

@app.route('/logout')
def show_logout():
    return redirect(url_for('show_home'))

@app.route('/getcookie')
def get_cookie():
    email = request.cookies.get('email_user')
    return 'Email kamu adalah ' + email

ALLOWED_EXTENTION = set(['png', 'jpg', 'jpeg'])
app.config['UPLOAD_FOLDER'] = 'uploads'

def allowed_file(filename):
    return '.' in filename and filename.split('.', 1)[1].lower() in ALLOWED_EXTENTION
    
@app.route('/upload', methods = ['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        file = request.files['file']
        
        if 'file' not in request.files:
            return redirect(request.url)
        
        if file.filename == '':
            return redirect(request.url)
        
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            return 'file berhasil disimpan pada ' + filename
        
    return render_template('upload.html')

@app.route("/download/<filename>", methods=['GET'])
def downloadfile(filename):
    return render_template('download.html', value=filename)

@app.route("/getdownload/<filename>")
def get_download(filename):
    file_path = 'uploads', filename
    return send_file(file_path, as_attachment=True, attachment_filename='')



