from flask import Flask, render_template, request, session, url_for, redirect
from flask_mysqldb import MySQL, MySQLdb
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField, Form, validators
from wtforms.validators import ValidationError, DataRequired, Email, EqualTo, Length

app = Flask(__name__)
app.secret_key = '6bb4e48163d4437da51e0389194eb89e'

app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = '6402'
app.config['MYSQL_DB'] = 'flask2'

mysql = MySQL(app)

class CrateUserForm(Form):
    name = StringField("name", validators=[validators.Length(min=5, max=25), validators.DataRequired(message="Nama terlalu panjang")])
    email = StringField('email', [validators.Length(min=6, max=35)])
    password = PasswordField('password', [
        validators.DataRequired()
    ])
    
    def validate_name(name):
        reg_char = '!3543*()'
        for char in name.data:
            if char in reg_char:
                raise ValidationError("Tidak boleh memasukkan speciall karakter")




@app.route('/')
def home():
    return render_template("home.html")

# @app.route('/register', methods = ['GET', 'POST'])
# def register():
#     if request.method == 'GET':
#         return render_template ('register.html')
#     else:
#         name = request.form['name']
#         email = request.form['email']
#         password = request.form['password'].encode('utf-8')
        
#         cur = mysql.connection.cursor()
#         cur.execute("INSERT INTO user (name, email, password) values(%s, %s, %s)", (name, email, password))
#         mysql.connection.commit()
#         session['name'] = request.form['name']
#         session['email'] = request.form['email']
#         return redirect (url_for('home'))

@app.route('/register', methods = ['GET', 'POST'])
def register():
    form = CrateUserForm(request.form)
    if request.method == 'POST' and form.validate():    
        name = form.name.data
        email = form.email.data
        password = form.password.data
        
        cur = mysql.connection.cursor()
        cur.execute("INSERT INTO user (name, email, password) values(%s, %s, %s)", (name, email, password))
        mysql.connection.commit()
        session['name'] = form.name.data
        session['email'] = form.email.data
        return redirect (url_for('home'))
    
    return render_template('register.html', form=form)
        

@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        email = request.form['email']
        password = request.form['password']
        
        curl = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
        curl.execute("SELECT * FROM user WHERE email=%s", (email,))
        user = curl.fetchone()
        curl.close()
        
        if (user["password"] == user["password"]):
            session['name'] = user['name']
            session['email'] = user['email']
            return render_template('dashboard.html')
        else :
            return 'Error password atau email salah'
    
    else :
        return render_template('login.html')

@app.route('/logout', methods = ['GET', 'POST'])
def logout():
    session.clear()
    return render_template('home.html')



@app.route('/dashboard')
def show_dashboard():
    cur = mysql.connection.cursor()
    cur.execute('SELECT * FROM product')
    rv = cur.fetchall()
    cur.close()
    return render_template('dashboard.html', products=rv)

@app.route('/tambah', methods=['POST'])
def tambah_data():
    nama = request.form['nama']
    cur = mysql.connection.cursor()
    cur.execute("INSERT INTO product (nama_barang) VALUES(%s)", (nama,))
    mysql.connection.commit()
    return redirect(url_for('show_dashboard'))

@app.route('/update', methods=['POST'])
def edit_data():
    id_data = request.form['id']
    nama = request.form['nama']
    cur = mysql.connection.cursor()
    cur.execute('UPDATE product SET nama_barang=%s WHERE id=%s', (nama, id_data))
    mysql.connection.commit()
    return redirect(url_for('show_dashboard'))

@app.route('/hapus/<string:id_data>', methods=['GET'])
def hapus(id_data):
    cur = mysql.connection.cursor()
    cur.execute('DELETE FROM product WHERE id=%s', (id_data))
    mysql.connection.commit()
    return redirect(url_for('show_dashboard'))

if __name__ == '__main__':
    app.run(debug=True)